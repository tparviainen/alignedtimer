/*
  Timer

  AlignedTimer API implementation

  Copyright (c) 2015 Tomi Parviainen. All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  See the file COPYING included with this distribution for more
  information.
*/

#include "AlignedTimer.h"

// Variable that holds the timestamp when the timer expired previously
static unsigned long prevExpirationTime = 0;

AlignedTimer::AlignedTimer() {
  for (int ind = 0; ind < TIMER_COUNT; ind++) {
    clear(&timerQ[ind]);
  }
}

int AlignedTimer::add(timerFn func, unsigned long delayInSeconds, unsigned long count) {
  timer_t timer;
  timer.delayInSeconds = delayInSeconds;
  timer.count = count;
  timer.func = func;
  return add(&timer);
}

int AlignedTimer::add(const timer_t* timer) {
  // Loop through to find first free timer
  for (int ind = 0; ind < TIMER_COUNT; ind++) {
    if (timerQ[ind].id == -1) {
      // Initialize timer for use
      timerQ[ind].id = ind;
      timerQ[ind].delayInSeconds = timer->delayInSeconds;
      // Callback must be called *after* delayInSeconds i.e.
      // need to add 1 sec to fulfil that requirement.
      timerQ[ind].delayCounter = timer->delayInSeconds + 1;
      timerQ[ind].count = timer->count;
      timerQ[ind].func = timer->func;
      return timerQ[ind].id;
    }
  }

  // No space in timer queue
  return -1;
}

int AlignedTimer::remove(int& id) {
  for (int ind = 0; ind < TIMER_COUNT; ind++) {
    if (timerQ[ind].id == id) {
      // Clear the timer and invalidate the ID
      clear(&timerQ[ind]);
      id = -1;

      return 0;
    }
  }

  return -1;
}

void AlignedTimer::run() {
  // Timer resolution is fixed to 1s (1000ms)
  if (millis() - prevExpirationTime >= 1000) {
    prevExpirationTime += 1000;

    for (int ind = 0; ind < TIMER_COUNT; ind++) {
      timer_t& timer = timerQ[ind];

      if (timer.id == -1) {
        continue;
      }

      // Check whether timer expires now
      if (--timer.delayCounter == 0) {
        timer.delayCounter = timer.delayInSeconds;

        timerFn func = timer.func;

        // Remove timer unless it is permanent!
        if (timer.count != RUN_FOREVER) {
          if (--timer.count == 0) {
            clear(&timer);
          }
        }

        (*func)();
      }
    }
  }
}

void AlignedTimer::clear(timer_t* timer) {
  timer->id = -1;
  timer->delayInSeconds = 0;
  timer->delayCounter = 0;
  timer->count = 0;
  timer->func = NULL;
}
