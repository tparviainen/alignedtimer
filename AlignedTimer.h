/*
  Timer

  AlignedTimer API

  Copyright (c) 2015 Tomi Parviainen. All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.
  See the file COPYING included with this distribution for more
  information.
*/

#ifndef ALIGNEDTIMER_H
#define ALIGNEDTIMER_H

#include "Arduino.h"

// Maximum amount of SW timers by default
#ifndef TIMER_COUNT
#define TIMER_COUNT 10
#endif // TIMER_COUNT

// Count value for timers that do not expire
const unsigned long RUN_FOREVER = -1;

// Function prototype for timers
typedef void (*timerFn)();

typedef struct {
  // Timer function pointer
  timerFn func;

  // Delay after the callback is called
  unsigned long delayInSeconds;

  // Counter how many times timer expires before it
  // will be removed from timer queue.
  unsigned long count;

  // Internal functionality ...
  int id;
  unsigned long delayCounter;
} timer_t;

class AlignedTimer
{
  public:
    AlignedTimer();

    // Adds timer to timer queue, returns unique timer ID
    int add(timerFn func, unsigned long delayInSeconds, unsigned long count);
    int add(const timer_t* timer);

    // Removes timer from the timer queue
    int remove(int& id);

    // Must be called from loop() to run expired timers
    void run();

  private:
    void clear(timer_t* timer);

  private:
    timer_t timerQ[TIMER_COUNT];
};

#endif // ALIGNEDTIMER_H
