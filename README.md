# AlignedTimer v1.1.0#

### Introduction ###
AlignedTimer is a timer library for Arduino. Aligned timers have a resolution of 1 sec and the callback functions are run on 1 sec boundaries. The difference between normal timers and aligned timers (see picture below) is that aligned timers are executed at 1 sec boundaries and the callback functions for several timers are aligned (if they expire at the same time). Normal timers on the other hand are independent of each other and the callback functions are not aligned between different timers. Example below shows how callback functions (red/blue circles) are called for periodic 1 sec normal and aligned timers.

![AlignedTimer.png](https://bitbucket.org/repo/yzp5Ao/images/3530152614-AlignedTimer.png)

As described in the picture the first time aligned timer expires is always equal or greater than the requested time in seconds (dash + solid horizontal red/blue lines) but the consecutive calls are executed exactly with the requested delay (solid horizontal red/blue lines). The resolution of the aligned timer is 1 sec.

### Installation ###
1. Download the latest AlignedTimer package from [here](https://bitbucket.org/tparviainen/alignedtimer/downloads/AlignedTimer.zip).
2. Start Arduino IDE and select **Sketch > Include Library > Add .ZIP Library...**
3. Select the library from the location where you downloaded it
4. Restart Arduino IDE (needed for examples / syntax highlight to work properly)

After you have installed the library there should be several examples in sketchbook for you to learn how to use AlignedTimer library.

### Usage ###
Please check the examples in sketchbook, but in briefly the usage goes in next way:

```
#!c++
#include <AlignedTimer.h>

// AlignedTimer class for using timer functionality
AlignedTimer at;

void setup() {
  // Add 1 sec timer that runs forever
  at.add(timerFunc, 1, RUN_FOREVER);
}

void loop() {
  // Must be called from loop() to run expired timers
  at.run();
}

void timerFunc() {
  // AlignedTimer callback function that is executed once a second ...
}
```
Please note that the function prototype for timer functions is *void functionName(void)* i.e. callback function does not take any parameters and does not return any value.


### Memory consumption ###
Memory footprint of AlignedTimer is very small. The flash consumption increases ~600 bytes when the library is used by the sketch. The RAM consumption increases 164 bytes due to the hardcoded size allocation for 10 timers (the size of timer_t is 16 bytes).


### Who do I talk to? ###
If you have questions related to AlignedTimer API or how to use it you can contact me via email (tomi.parviainen@gmail.com).