/*
  Simple2.ino

  Example of how to do a 1 sec periodic timer that runs forever

  Copyright (c) 2015 Tomi Parviainen. All rights reserved.
*/
#include <AlignedTimer.h>

// AlignedTimer class for using timer functionality
AlignedTimer at;

void setup() {
  // Initialize serial and wait for port to open
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  // Add 1 sec timer that runs forever
  timer_t timer;
  timer.func = timerFunc;
  timer.delayInSeconds = 1;
  timer.count = RUN_FOREVER;

  at.add(&timer);
}

void loop() {
  // Must be called from loop() to run expired timers
  at.run();
}

void timerFunc() {
  Serial.println(F("1 sec AlignedTimer"));
}
