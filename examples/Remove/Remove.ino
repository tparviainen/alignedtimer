/*
  Remove.ino

  Example of how to remove a timer

  Copyright (c) 2015 Tomi Parviainen. All rights reserved.
*/
#include <AlignedTimer.h>

// AlignedTimer class for using timer functionality
AlignedTimer at;

// Unique timer ID
int id = -1;

void setup() {
  // Initialize serial and wait for port to open
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  // Add 1 sec timer that runs forever (or until removed!)
  id = at.add(timerFunc, 1, RUN_FOREVER);
}

void loop() {
  // Must be called from loop() to run expired timers
  at.run();
}

void timerFunc() {
  Serial.println(F("1 sec AlignedTimer"));

  // Remove timer
  at.remove(id);
}
