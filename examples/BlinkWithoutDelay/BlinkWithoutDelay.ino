/*
  BlinkWithoutDelay.ino

  Turns on and off a light emitting diode (LED) connected to a digital
  pin, without using the delay() function.

  Copyright (c) 2015 Tomi Parviainen. All rights reserved.
*/
#include <AlignedTimer.h>

// AlignedTimer class for using timer functionality
AlignedTimer at;

// The number of the LED pin on Arduino Uno
const int LED_PIN = 13;

void setup() {
  // Initialize serial and wait for port to open
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  // Set the digital pin as output
  pinMode(LED_PIN, OUTPUT);

  // Add 1 sec timer that runs forever
  at.add(timerFunc, 1, RUN_FOREVER);
}

void loop() {
  // Must be called from loop() to run expired timers
  at.run();
}

void timerFunc() {
  static int ledState = LOW;

  // Swap the LED state every time this function gets called
  if (ledState == LOW) {
    ledState = HIGH;
  } else {
    ledState = LOW;
  }

  digitalWrite(LED_PIN, ledState);
}
