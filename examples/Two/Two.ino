/*
  Two.ino

  Example of how to do two timers

  Copyright (c) 2015 Tomi Parviainen. All rights reserved.
*/
#include <AlignedTimer.h>

// AlignedTimer class for using timer functionality
AlignedTimer at;

void setup() {
  // Initialize serial and wait for port to open
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  // Add 1 sec timer that runs forever
  at.add(timerFunc1, 1, RUN_FOREVER);

  // Add 2 sec timer that runs forever
  at.add(timerFunc2, 2, RUN_FOREVER);
}

void loop() {
  // Must be called from loop() to run expired timers
  at.run();
}

void timerFunc1() {
  Serial.println(F("1 sec AlignedTimer"));
}

void timerFunc2() {
  Serial.println(F("2 sec AlignedTimer"));
}
