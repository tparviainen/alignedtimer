/*
  Simple3.ino

  Example of how to do a 3 sec periodic timer that expires after 10 times

  Copyright (c) 2015 Tomi Parviainen. All rights reserved.
*/
#include <AlignedTimer.h>

// AlignedTimer class for using timer functionality
AlignedTimer at;

void setup() {
  // Initialize serial and wait for port to open
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  // Add 3 sec timer that runs 10 times and stops
  at.add(timerFunc, 3, 10);
}

void loop() {
  // Must be called from loop() to run expired timers
  at.run();
}

void timerFunc() {
  static int count = 0;
  Serial.print(F("3 sec AlignedTimer, count = "));
  Serial.println(++count);
}
