/*
  Watchdog.ino

  Example of how to do a periodic timer that feeds watchdog timer (WDT)

  Copyright (c) 2015 Tomi Parviainen. All rights reserved.
*/
#include <avr/wdt.h>
#include <AlignedTimer.h>

// AlignedTimer class for using timer functionality
AlignedTimer at;

void setup() {
  // Initialize serial and wait for port to open
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  // Add 5 sec WDT feed timer that runs forever
  at.add(reset_wdt, 5, RUN_FOREVER);

  // Enable 8s watchdog
  wdt_enable(WDTO_8S);
}

void loop() {
  // Must be called from loop() to run expired timers
  at.run();
}

void reset_wdt() {
  // Reset the watchdog timer
  wdt_reset();
}
